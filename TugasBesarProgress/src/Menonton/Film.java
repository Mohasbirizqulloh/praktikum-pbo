package Menonton;
import java.util.Scanner;

public class Film extends Konten {
    public Film(String title,String genre, int durasi, int tahun, String sutradara){
        super(title, genre, durasi, tahun, sutradara);
    }

    public void tampilkanInfoFilm(){
        System.out.println("\n  <<<<Informasi Film>>>> ");
        System.out.println("---------------------------");
        System.out.println("Judul Film\t: " + getTitle());
        System.out.println("Genre\t\t: " + getGenre());
        System.out.println("Sutradara\t: " + getSutradara());
        System.out.println("Tahun\t\t: " + getTahun());
        System.out.println("Durasi\t\t: " + getDurasi() + " Menit");
        System.out.println("Rating\t\t: " + getRating());
        System.out.println("----------------------------");
    }

    public void tambahRating(double buatRating) {
        double jumlahRating = getRating() * getJumlahPenonton();
        setJumlahPenonton(getJumlahPenonton() + 1);
        setRating((jumlahRating + buatRating) / getJumlahPenonton());
    }

    @Override
    public void watch(){
        Scanner input = new Scanner(System.in);
        System.out.print("Apakah anda ingin menonton film ini? (y/n): ");
        String pilihan = input.nextLine();
        if (pilihan.equals("y")) {
            System.out.print("\n\t\t  Streaming FILM");
            System.out.println("\n==================================================");
            System.out.println(" Menonton film " + getTitle() +  " Genre film " + getGenre() + "  ");
            System.out.println(" \t\t    " + getDurasi() + " Menit \t\t\t ");
            System.out.println("==================================================\n");
            System.out.print("Beri rating untuk film ini (1-10): ");
                int rating = input.nextInt();
                tambahRating(rating);
        } else {
            System.out.println("\nMembatalkan menonton " + getTitle());
            System.out.println("LOADING .....");
            System.out.println("Kembali ke menu utama .....");
        }
    }

    public void watch(String subtitle){
        Scanner input = new Scanner(System.in);
        System.out.print("Apakah anda ingin menonton film ini? (y/n): ");
        String pilihan = input.nextLine();
        if (pilihan.equals("y")) {
            System.out.print("\n\t\t  Streaming FILM");
            System.out.println("\n==================================================");
            System.out.println(" Menonton film " + getTitle() +  " Genre film " + getGenre() + "  ");
            System.out.println(" \t      Subtitle Bahasa " + subtitle + "\t\t ");
            System.out.println(" \t\t    " + getDurasi() + " Menit \t\t\t ");
            System.out.println("==================================================\n");
            System.out.print("Beri rating untuk film ini (1-10): ");
                int rating = input.nextInt();
                tambahRating(rating);
        } else {
            System.out.println("\nMembatalkan menonton " + getTitle());
            System.out.println("LOADING .....");
            System.out.println("Kembali ke menu utama .....");
        }
    }
}
