package Menonton;

public abstract class Konten {
    private String title;
    private String genre;
    private int durasi;
    private int tahun;
    private String sutradara;
    private double rating;
    private int jumlahPenonton;

    public Konten(String title, String genre, int durasi, int tahun, String sutradara){
        this.title = title;
        this.genre = genre;
        this.durasi = durasi;
        this.tahun = tahun;
        this.sutradara = sutradara;
        this.rating = 0;
        this.jumlahPenonton = 0;
    }

    public String getTitle(){
        return this.title;
    }

    public String getGenre(){
        return this.genre;
    }

    public int getDurasi(){
        return this.durasi;
    }

    public int getTahun(){
        return this.tahun;
    }

    public String getSutradara(){
        return this.sutradara;
    }

    public double getRating(){
        return this.rating;
    }

    public void setRating(double isiRating){
        this.rating = isiRating;
    }

    public void setJumlahPenonton(int isiSetJumlahPenonton){
        this.jumlahPenonton = isiSetJumlahPenonton;
    }

    public int getJumlahPenonton(){
        return this.jumlahPenonton;
    }

    public abstract void watch();
}
