package Menonton;
import java.util.Scanner;

public class SerialTv extends Konten{
    private int episode;
    public SerialTv(String title, String genre, int durasi, int tahun, String sutradara, int episode){
        super(title, genre, durasi, tahun, sutradara);
        this.episode = episode;
    }

    public int getEpisode(){
        return this.episode;
    }

    public void setEpisode(int inputEpisode){
        this.episode = inputEpisode;
    }

    public void tampilkanInfoSerial(){
        System.out.println("\n  <<<<Informasi Serial>>>> ");
        System.out.println("---------------------------");
        System.out.println("Judul Film\t: " + getTitle());
        System.out.println("Genre\t\t: " + getGenre());
        System.out.println("Sutradara\t: " + getSutradara());
        System.out.println("Tahun\t\t: " + getTahun());
        System.out.println("Durasi\t\t: " + getDurasi() + " Menit");
        System.out.println("Episode\t\t: " + getEpisode() + " Episode");
        System.out.println("Rating\t\t: " + getRating());
        System.out.println("----------------------------");
    }

    public void tambahRating(double buatRating) {
        double jumlahRating = getRating() * getJumlahPenonton();
        setJumlahPenonton(getJumlahPenonton() + 1);
        setRating((jumlahRating + buatRating) / getJumlahPenonton());
    }

    @Override
    public void watch(){
        Scanner input = new Scanner(System.in);
        System.out.print("Apakah anda ingin menonton Serial ini? (y/n): ");
        String pilihan = input.nextLine();
        System.out.print("Episode berapa: ");
        int epsd = input.nextInt();
        setEpisode(epsd);
        if (pilihan.equals("y")) {
            System.out.print("\n\t\t  Streaming Serial");
            System.out.println("\n=======================================================");
            System.out.println("Menonton film " + getTitle() +  " Genre " + getGenre() + "  ");
            System.out.println("\t\tEpisode " + getEpisode());
            System.out.println(" \t\t     " + getDurasi() + " Menit \t\t\t ");
            System.out.println("=======================================================\n");
            System.out.print("Beri rating untuk film ini (1-10): ");
                int rating = input.nextInt();
                tambahRating(rating);
        } else {
            System.out.println("\nMembatalkan menonton " + getTitle());
            System.out.println("LOADING .....");
            System.out.println("Kembali ke menu utama .....");
        }
    }

    public void watch(String subtitle){
        Scanner input = new Scanner(System.in);
        System.out.print("Apakah anda ingin menonton Serial ini? (y/n): ");
        String pilihan = input.nextLine();
        if (pilihan.equals("y")) {
            System.out.print("\n\t\t  Streaming Serial");
            System.out.println("\n=======================================================");
            System.out.println(" Menonton film " + getTitle() +  " Genre " + getGenre() + "  ");
            System.out.println("\t\t\tEpisode " + getEpisode());
            System.out.println(" \t\t    Subtitle Bahasa " + subtitle + "\t\t ");
            System.out.println(" \t\t         " + getDurasi() + " Menit \t\t\t ");
            System.out.println("=======================================================\n");
            System.out.print("Beri rating untuk film ini (1-10): ");
                int rating = input.nextInt();
                tambahRating(rating);
        } else {
            System.out.println("\nMembatalkan menonton " + getTitle());
            System.out.println("LOADING .....");
            System.out.println("Kembali ke menu utama .....");
        }
    }
}
