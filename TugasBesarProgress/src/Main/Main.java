package Main;
import java.util.Scanner;
import DataFilm.KumpulanFilm;
import DataFilm.KumpulanSerial;
import Menonton.Film;
import Menonton.SerialTv;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Menu menu = new Menu();
        KumpulanFilm dataFilm = new KumpulanFilm();
        KumpulanSerial dataSerial = new KumpulanSerial();

        int pilihan;
        do {
            menu.tampilkanMenu();
            pilihan = menu.getInput();

            switch (pilihan) {
                case 1:
                    dataFilm.tampilkanKumpulanFilm();
                    dataSerial.tampilkanKumpulanSerial();

                    // Inputan untuk memilih Film atau Serial
                    System.out.println("\nPilih salah satu opsi berikut:");
                    System.out.println("1. Tonton Film");
                    System.out.println("2. Tonton Serial TV");
                    System.out.print("===> ");
                    int pilihTonton = input.nextInt();
                    if (pilihTonton == 1) {
                        System.out.print("\nPilih nomor film yang ingin ditonton: ");
                        int pilihFilm = input.nextInt();
                        Film filmPilihan = dataFilm.getFilm(pilihFilm - 1);
                        filmPilihan.tampilkanInfoFilm();
                        System.out.print("Apakah Anda ingin menonton dengan subtitle? (Y/N) ==> ");
                        String pilihSubtitle = input.next();
                        if (pilihSubtitle.equalsIgnoreCase("Y")) {
                            System.out.print("Pilih subtitle (contoh: Indonesia): ");
                            String subtitle = input.next();
                            filmPilihan.watch(subtitle);
                        } else if (pilihSubtitle.equalsIgnoreCase("N")) {
                            filmPilihan.watch();
                        } else {
                            System.out.println("Pilihan tidak valid.");
                        }
                        
                    } else if (pilihTonton == 2) {
                        System.out.print("\nPilih nomor serial yang ingin ditonton: ");
                        int pilihSerial = input.nextInt();
                        SerialTv serialPilihan = dataSerial.getSerial(pilihSerial - 1);
                        serialPilihan.tampilkanInfoSerial();
                        System.out.print("Apakah Anda ingin menonton dengan subtitle? (Y/N) ==> ");
                        String pilihSubtitle = input.next();
                        if (pilihSubtitle.equalsIgnoreCase("Y")) {
                            System.out.print("Pilih subtitle (contoh: Indonesia): ");
                            String subtitle = input.next();
                            serialPilihan.watch(subtitle);
                        } else if (pilihSubtitle.equalsIgnoreCase("N")) {
                            serialPilihan.watch();
                        } else {
                            System.out.println("Pilihan tidak valid.");
                        }
                    } else {
                        System.out.println("Pilihan tidak valid.");
                    }
                    break;
                case 2:
                    System.out.print("\nMasukkan judul film atau serial yang ingin dicari: ");
                    String judul = input.nextLine();
                
                    // Mencari film berdasarkan judul
                    Film dicariFilm = dataFilm.cariFilm(judul);
                    if (dicariFilm != null) {
                        dicariFilm.tampilkanInfoFilm();
                        System.out.print("Apakah Anda ingin menonton dengan subtitle? (Y/N) ==> ");
                        String pilihSubtitle = input.next();
                        if (pilihSubtitle.equalsIgnoreCase("Y")) {
                            System.out.print("Pilih subtitle (contoh: Indonesia): ");
                            String subtitle = input.next();
                            dicariFilm.watch(subtitle);
                        } else if (pilihSubtitle.equalsIgnoreCase("N")) {
                            dicariFilm.watch();
                        } else {
                            System.out.println("Pilihan tidak valid.");
                        }
                    }
                
                    // Mencari serial berdasarkan judul
                    SerialTv serialPilihan = dataSerial.cariSerial(judul);
                    if (serialPilihan != null) {
                        serialPilihan.tampilkanInfoSerial();
                        System.out.print("Apakah Anda ingin menonton dengan subtitle? (Y/N) ==> ");
                        String pilihSubtitle = input.next();
                        if (pilihSubtitle.equalsIgnoreCase("Y")) {
                            System.out.print("Pilih subtitle (contoh: Indonesia): ");
                            String subtitle = input.next();
                            serialPilihan.watch(subtitle);
                        } else if (pilihSubtitle.equalsIgnoreCase("N")) {
                            serialPilihan.watch();
                        } else {
                            System.out.println("Pilihan tidak valid.");
                        }
                    }
                    
                    // Jika tidak ada film atau serial yang ditemukan
                    if (dicariFilm == null && serialPilihan == null ) {
                        System.out.println("Film atau serial tidak ditemukan.");
                    }
                    break;
                case 3:
                    System.out.println("\n<<<Terima Kasih Telah Menggunakan Aplikasi Kami>>>\n");
                    break;
                default:
                    break;
            }
        } while (pilihan != 3);  
    }
    
}