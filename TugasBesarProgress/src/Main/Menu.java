package Main;

import java.util.Scanner;

public class Menu {
    private Scanner input;

    public Menu(){
        this.input = new Scanner(System.in);
    }

    public void tampilkanMenu(){
        System.out.println("\n==========================");
        System.out.println("        MENU UTAMA        ");
        System.out.println("==========================");
        System.out.println("1. Tampilkan data film dan serial TV");
        System.out.println("2. Pencarian Film atau Serial");
        System.out.println("3. Keluar");
        System.out.print("Input Pilihan Menu:  ");
    }

    public int getInput(){
        return input.nextInt();
    }
}
