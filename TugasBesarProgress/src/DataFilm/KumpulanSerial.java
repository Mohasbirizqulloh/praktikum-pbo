package DataFilm;
import java.util.ArrayList;
import Menonton.SerialTv;
import java.util.Collections;
import java.util.Comparator;

public class KumpulanSerial {
    private ArrayList<SerialTv> kumpulanSerial = new ArrayList<>();

    public KumpulanSerial(){
        kumpulanSerial.add(new SerialTv("Stranger Things", "Drama, Horror, Mystery", 42, 2016, "Matt Duffer, Ross Duffer", 25));
        kumpulanSerial.add(new SerialTv("The Crown", "Drama, History", 47, 2016, "Peter Morgan",50));
        kumpulanSerial.add(new SerialTv("Narcos", "Crime, Drama, Thriller",49, 2015, "Chris Brancato, Carlo Bernard, Doug Miro",30));
        kumpulanSerial.add(new SerialTv("Bridgerton", "Drama, Romance",57, 2020, "Chris Van Dusen",8));
        kumpulanSerial.add(new SerialTv("Black Mirror", "Drama, Sci-Fi, Thriller",41, 2011, "Charlie Brooker",22));
        kumpulanSerial.add(new SerialTv("The Witcher", "Petualangan, Fantasi, Aksi",47, 2019, "Lauren Schmidt Hissrich",16));
        kumpulanSerial.add(new SerialTv("Money Heist", "Drama, Kejahatan, Thriller",45, 2017, "Álex Pina",40));
        kumpulanSerial.add(new SerialTv("Mindhunter ", "Drama, Kejahatan, Thriller",34, 2017, "David Fincher",19));
        kumpulanSerial.add(new SerialTv("The Umbrella Academy", "Petualangan, Fantasi, Aksi",42, 2019, "Steve Blackman",20));
        kumpulanSerial.add(new SerialTv("Orange Is the New Black", "Drama, Komedi, Kejahatan",50, 2013, "Jenji Kohan",91));

        // Mengurutkan kumpulanFilm berdasarkan judul
        Collections.sort(kumpulanSerial, new Comparator<SerialTv>() {
          public int compare(SerialTv s1, SerialTv s2) {
              return s1.getTitle().compareToIgnoreCase(s2.getTitle());
          }
      });
    }

    
    public void tampilkanKumpulanSerial() {
      System.out.println("\n<<<Kumpulan Serial>>>");
      for (int i = 0; i < kumpulanSerial.size(); i++) {
        System.out.println(i+1 + ". " + kumpulanSerial.get(i).getTitle());
      }
    }

    public SerialTv cariSerial(String namaSerial) {
      // Mencari film dengan binary search
       int awal = 0, akhir = kumpulanSerial.size()-1;
       while (awal <= akhir) {
         int tengah = (awal + akhir) / 2;
         SerialTv serial = kumpulanSerial.get(tengah);
         int cmp = serial.getTitle().compareToIgnoreCase(namaSerial);
         if (cmp == 0) {
             return serial;
         } else if (cmp < 0) {
             awal = tengah + 1;
         } else {
             akhir = tengah - 1;
         }
     }
       return null;
   }

    public SerialTv getSerial(int indeks){
      return kumpulanSerial.get(indeks);
    }
  }
  