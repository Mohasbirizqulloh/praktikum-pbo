package DataFilm;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import Menonton.Film;


public class KumpulanFilm {
    private ArrayList<Film> kumpulanFilm = new ArrayList<>();
    public KumpulanFilm(){
        kumpulanFilm.add(new Film("The Irishman", "Drama, Kejahatan", 209 , 2019, "Martin Scorsese"));
        kumpulanFilm.add(new Film("The Trial of the Chicago 7", "Drama, Sejarah",130, 2020, "Aaron Sorkin"));
        kumpulanFilm.add(new Film("The Old Guard ", "Petualangan, Aksi",125, 2020, "Gina Prince-Bythewood"));
        kumpulanFilm.add(new Film("Mudbound", "Drama, Sejarah",135, 2017, "Dee Rees"));
        kumpulanFilm.add(new Film("Beasts of No Nation", "Drama, Perang",137 , 2015, "Cary Joji Fukunaga"));
        kumpulanFilm.add(new Film("John Wick", "Aksi, Thriller",101  , 2014, "Chad Stahelski, David Leitch"));
        kumpulanFilm.add(new Film("Inception", "Aksi, Petualangan, Sci-Fi",148  , 2010, "Christopher Nolan"));
        kumpulanFilm.add(new Film("The Eternals", "Aksi, Petualangan, Fantasi",157  , 2021, "Chloé Zhao"));
        kumpulanFilm.add(new Film("Venom: Let There Be Carnage", "Aksi, Petualangan, Kejahatan",97  , 2021, "Andy Serkis"));
        kumpulanFilm.add(new Film("Pacific Rim", "Aksi, Petualangan, Fantasi",131  , 2013, "Guillermo del Toro"));

        // Mengurutkan kumpulanFilm berdasarkan judul
        Collections.sort(kumpulanFilm, new Comparator<Film>() {
          public int compare(Film f1, Film f2) {
              return f1.getTitle().compareToIgnoreCase(f2.getTitle());
          }
      });
    }

    public void tampilkanKumpulanFilm() {
        System.out.println("\n<<<Kumpulan Film>>>");
        for (int i = 0; i < kumpulanFilm.size(); i++) {
          System.out.println(i+1 + ". " + kumpulanFilm.get(i).getTitle());
        }
      }

    public Film cariFilm(String namaFilm) {
       // Mencari film dengan binary search
        int awal = 0, akhir = kumpulanFilm.size()-1;
        while (awal <= akhir) {
          int tengah = (awal + akhir) / 2;
          Film film = kumpulanFilm.get(tengah);
          int cmp = film.getTitle().compareToIgnoreCase(namaFilm);
          if (cmp == 0) {
              return film;
          } else if (cmp < 0) {
              awal = tengah + 1;
          } else {
              akhir = tengah - 1;
          }
      }
        return null;
    }

    public Film getFilm(int indeks){
      return kumpulanFilm.get(indeks);
    }
}
