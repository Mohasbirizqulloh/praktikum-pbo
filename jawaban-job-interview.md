# No 1
Dalam projek saya dalam penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman, saya menerapkannya pada method tambahRating. disini saya dapat menghitung rating yang diberikan oleh penonton terhadap film yang sudah ditonton. Selanjutnya pada method cariFilm dan cariSeries saya menggunakan algoritma binary search agar bisa mencari judul film sesuai dengan inputan dari user.
![Jawaban-nomer-satu](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Nosatu.gif)
# No 2
Algoritma yang saya gunakan adalah binary search untuk mencari judul film yang diinginkan oleh user.Algoritma binary search mengambil nilai tengah dari array dan membandingkannya dengan elemen yang dicari. Jika nilai tengah sama dengan elemen yang dicari, pencarian selesai. Jika nilai tengah lebih besar dari elemen yang dicari, maka pencarian dilakukan di setengah kiri array. Jika nilai tengah lebih kecil dari elemen yang dicari, maka pencarian dilakukan di setengah kanan array. Proses ini diulang hingga elemen yang dicari ditemukan atau array tidak dapat dibagi lagi.
![Jawaban-nomer-dua](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Nodua.gif)
# No 3
Dalam projek yang saya buat sudah terdapat implementasi dari oop yang sudah saya pelajari sebelumnya. Oop adalah paradigma pemrograman yang berfokus pada penggunaan objek dan kelas untuk merancang dan membangun aplikasi. Konsep dasar OOP meliputi Encapsulation, Inheritance, Polymorphism, dan Abstraction.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Notiga.gif)
# No 4
implementasi dari Encapsulation yang saya terapkan pada program saya yaitu pada setiap atribut pada objek yang sudah saya buat berikut ini. Encapsulation digunakan untuk menyembunyikan detail implementasi dari suatu objek atau kelas, dan hanya mengekspos fungsi-fungsi publik untuk interaksi dengan objek atau kelas tersebut. Dalam encapsulation, data dan fungsi terkait dikemas bersama dalam sebuah unit yang disebut kelas. Tujuan utama dari encapsulation adalah untuk menjaga integritas data dan mencegah penggunaan yang salah atau tidak sah dari data atau fungsi tersebut. Contohnya seperti data-data atribut dalam class Konten film dan series tentunya data-data tersebut tidak boleh lagi diubah.
![Jawaban-nomer-empat](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Noempat.gif)
# No 5
Class Konten menjadi sebuah Abstraction sehingga kita dapat membuat program yang lebih modular dan mudah diatur dengan mengelompokkan konten-konten menjadi satu kelas abstrak yang memiliki properti dan metode yang sama. Class konten nantinya berfungsi sebagai wadah yang menampung data dari childnya. Kita juga dapat menambahkan perilaku atau properti khusus pada setiap kelas turunan sesuai dengan kebutuhan aplikasi. Kelas turunan dari Konten yaitu class Film dan SeriesTv, nantinya akan mengambil semua method dan atribut dalam class parentnnya dan akan memiliki sifat unik masing-masing dalam class childnya.
![Jawaban-nomer-lima](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Nolima.gif)
# No 6
Disini saya menggunakan Inheritance pada package menonton, disana terdapat beberapa class yang sudah saya buat. Class Konten menjadi parentnya karena disini memuat atribut-atribut yang bisa diturunkan ke childnya nanti. Class Film dan SeriesTv menjadi childnya dan akan mengextends parentnya untuk mendapatkan sifat-sifat yang dimiliki oleh parentnya agar bisa digunakan oleh childnya. sedangkan untuk Polymorphism yaitu pada method abstrak watch yang nantinya akan di override oleh childnya sehingga memiliki sifat khusus dan unik.
![Jawaban-nomer-enam](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Noenam.gif)
# No 7
Proses bisnis yang terdapat pada projek saya adalah aplikasi streaming video yang memungkinkan pengguna untuk menonton film dan serial TV. Berikut adalah deskripsi lengkapnya :
- Pengguna dapat menonton konten yang tersedia di dalam platform. dalam kodinga saya, use case ini sudah ada dalam class Film dan SerialTv.
- Pengguna dapat Memberikan ulasan dan rating pada konten yang telah ditonton. use case ini terdapat pada class Film dan juga SerialTv yang berfungsi untuk memberikan rating konten yang sudah ditonton.
- Mencari film atau serial TV berdasarkan judul.
Pengguna dapat mencari film atau serial TV berdasarkan judul yang diinginkan. Jika ditemukan, pengguna dapat memilih apakah ingin menonton dengan atau tanpa subtitle. dalam kodinngan saya terdapat pada class kumpulanFilm dan kumpulanSerial.
- Menampilkan informasi film atau serial TV.
Pada saat memilih film atau serial TV, pengguna dapat melihat informasi tentang film atau serial TV tersebut, seperti judul, tahun rilis, dan sinopsis. dalam kodingan saya terdapat pada class Film dan Serial disana terdapat method tampilkanInfoFilm dan tampilkanInfoSeries.
- Menonton film atau serial TV dengan subtitle.
Jika pengguna memilih untuk menonton dengan subtitle, aplikasi akan menampilkan pilihan subtitle yang tersedia dan menampilkan subtitle yang dipilih selama film atau serial TV sedang diputar. Terdapat pada class Konten yang nantinya akan di override dan di overload pada class Film dan class SerialTv sebagai childnya.
# No 8
#### Class Diagram Netflix 
![Jawaban-nomer-delapan](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Untitled_Diagram.drawio__18_.png)
#### Use Case Table Netflix 
Key | Use Case | Prioritas | Status |
 --- | --- | --- | --- |
1 | Pengguna dapat menonton konten yang tersedia di dalam platform. | Tinggi | Completed |
2 | Pengguna dapat menyimpan konten film yang menarik untuk ditonton nanti dan menambahkannya ke dalam daftar list tontonan. | Sedang | Unplanned |
3 | Pengguna dapat mencari konten berdasarkan judul, genre, atau bintang film | Sedang | Planned |
4 | Menyediakan rekomendasi konten yang berdasarkan riwayat penonton dan rating yang diberikan oleh pengguna. | Sedang | Unplanned |
5 | Pengguna dapat menonton konten dengan akses offline | Rendah | Unplanned | 
6 | Pengguna dapat menonton konten dengan bahasa dan subtitle yang diinginkan | Sedang | Completed | 
7 | Pengguna dapat Memberikan ulasan dan rating pada konten yang telah ditonton | Sedang | Completed |
8 | Pengguna dapat mengirimkan saran atau feedback kepada Netflix | Rendah | Unplanned | 
9 | Pengguna dapat menggunakan fitur Fast Forward / Rewind untuk melompati bagian-bagian yang tidak diinginkan | Rendah | Unplanned |
10 | Pengguna dapat daftar berlangganan untuk melihat semua konten yang tersedia | Sedang | Unplanned |
11 | Menampilkan daftar film dan serial TV | Sedang | Completed |
12 | Menampilkan informasi film atau serial TV | Sedang | Completed |


# No 9
Link youtubbe demonstrasi gambaran umum aplikasi saya https://youtu.be/dnf5nCiKNHE
# No 10
Pada projek saya tampilannya menggunakan cmd seperti dibawah ini. Tetapi, rencananya saya akan membuat interfacenya lebih menarik lagi dengan membuatnya berbasis web kedepannya.
![Jawaban-nomer-sepuluh](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Nosepuluh.gif)
