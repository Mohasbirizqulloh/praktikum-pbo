import java.util.ArrayList;

class Movie {
  public String judul;
  public String genre;
  public int tahun;
  public String sutradara;

  public Movie(String judul, String genre, int tahun, String sutradara) {
    this.judul = judul;
    this.genre = genre;
    this.tahun = tahun;
    this.sutradara = sutradara;
  }
}

class User {
  public String nama;
  public int umur;
  public ArrayList<Movie> watchList;

  public User(String nama, int umur) {
    this.nama = nama;
    this.umur = umur;
    this.watchList = new ArrayList<Movie>();
  }

  public void addMovieToWatchList(Movie movie) {
    if (movie.genre.equals("18+")) {
      if (umur < 18) {
        System.out.println("Kamu belum cukup umur untuk menonton film ini !!.");
      } else {
        watchList.add(movie);
        System.out.println(movie.judul + " telah ditambahkan ke watch list.");
      }
    } else {
      watchList.add(movie);
      System.out.println(movie.judul + " telah ditambahkan ke watch list.");
    }
  }
}

public class ProjekOop {
  public static void main(String[] args) {
    Movie movie1 = new Movie("The Dark Knight", "Action", 2008, "Christopher Nolan");
    Movie movie2 = new Movie("Inception", "Sci-Fi", 2010, "Christopher Nolan");
    Movie movie3 = new Movie("The Shawshank Redemption", "Drama", 1994, "Frank Darabont");

    User user1 = new User("Marsudin", 25);
    User user2 = new User("Jandia", 16);

    user1.addMovieToWatchList(movie1);
    user1.addMovieToWatchList(movie2);
    user1.addMovieToWatchList(movie3);
    System.out.println(" ");
    user2.addMovieToWatchList(movie1);
    user2.addMovieToWatchList(movie2);
    user2.addMovieToWatchList(movie3);
    System.out.println(" ");
    
    System.out.println(user1.nama + "'s watch list:");
    for (Movie movie : user1.watchList) {
      System.out.println(movie.judul);
    }
    System.out.println(" ");
    System.out.println(user2.nama + "'s watch list:");
    for (Movie movie : user2.watchList) {
      System.out.println(movie.judul);
    }
  }
}