package ProgresTubes;

public class Movie {
    private String judul;
    private String genre;
    private int tahun;
    private String sutradara;

    public Movie(String judul, String genre, int tahun, String sutradara) {
        this.judul = judul;
        this.genre = genre;
        this.tahun = tahun;
        this.sutradara = sutradara;
    }

    public String getJudul() {
      return judul;
    }

    public String getGenre() {
      return genre;
    }

    public int getTahun() {
      return tahun;
    }

    public String getSutradara() {
      return sutradara;
    }
}
