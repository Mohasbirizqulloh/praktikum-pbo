package ProgresTubes;
import java.util.ArrayList;;

public class User extends Orang{
    private ArrayList<Movie> watchList;

    public User(String nama, int umur) {
        super(nama, umur);
        this.watchList = new ArrayList<Movie>();
    }

    public void addMovieToWatchList(Movie movie) {
        if (movie.getGenre().equals("18+")) {
          if (getUmur() < 18) {
            System.out.println("Kamu belum cukup umur untuk menonton film ini !!.");
          } else {
            watchList.add(movie);
            System.out.println(movie.getJudul() + " telah ditambahkan ke watch list.");
          }
        } else {
          watchList.add(movie);
          System.out.println(movie.getJudul() + " telah ditambahkan ke watch list.");
        }
      }
    
      public ArrayList<Movie> getWatchList() {
        return watchList;
      }

}
