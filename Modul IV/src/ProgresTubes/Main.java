package ProgresTubes;

public class Main{
  public static void main(String[] args) {
    Movie movie1 = new Movie("The Dark Knight", "Action", 2008, "Christopher Nolan");
    Movie movie2 = new Movie("Inception", "Sci-Fi", 2010, "Christopher Nolan");
    Movie movie3 = new Movie("The Shawshank Redemption", "Drama", 1994, "Frank Darabont");
    
    User user1 = new User("Marsudin", 25);
    User user2 = new User("Jandia", 16);

    user1.addMovieToWatchList(movie1);
    user1.addMovieToWatchList(movie2);
    user1.addMovieToWatchList(movie3);
       System.out.println(" ");
    user2.addMovieToWatchList(movie1);
    user2.addMovieToWatchList(movie2);
    user2.addMovieToWatchList(movie3);
    System.out.println(" ");

    System.out.println(user1.getNama() + "'s watch list:");
    for (Movie movie : user1.getWatchList()) {
        System.out.println(movie.getJudul());
    }
    System.out.println(" ");
    System.out.println(user2.getNama() + "'s watch list:");
    for (Movie movie : user2.getWatchList()) {
        System.out.println(movie.getJudul());
    }
  }
}