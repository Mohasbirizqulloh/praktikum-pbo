package ProgresTubes;

public class Orang {
    private String nama;
    private int umur;
  
    public Orang(String nama, int umur) {
      this.nama = nama;
      this.umur = umur;
    }
  
    public String getNama() {
      return nama;
    }
  
    public int getUmur() {
      return umur;
    }
}
