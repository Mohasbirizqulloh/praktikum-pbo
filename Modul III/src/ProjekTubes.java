import java.util.ArrayList;

class Movie {
  private String judul;
  private String genre;
  private int tahun;
  private String sutradara;

  public Movie(String judul, String genre, int tahun, String sutradara) {
    this.judul = judul;
    this.genre = genre;
    this.tahun = tahun;
    this.sutradara = sutradara;
  }

  public String getJudul() {
    return judul;
  }

  public String getGenre() {
    return genre;
  }

  public int getTahun() {
    return tahun;
  }

  public String getSutradara() {
    return sutradara;
  }
}

class User {
  private String nama;
  private int umur;
  private ArrayList<Movie> watchList;

  public User(String nama, int umur) {
    this.nama = nama;
    this.umur = umur;
    this.watchList = new ArrayList<Movie>();
  }

  public String getNama() {
    return nama;
  }

  public int getUmur() {
    return umur;
  }

  public void addMovieToWatchList(Movie movie) {
    if (movie.getGenre().equals("18+")) {
      if (umur < 18) {
        System.out.println("Kamu belum cukup umur untuk menonton film ini !!.");
      } else {
        watchList.add(movie);
        System.out.println(movie.getJudul() + " telah ditambahkan ke watch list.");
      }
    } else {
      watchList.add(movie);
      System.out.println(movie.getJudul() + " telah ditambahkan ke watch list.");
    }
  }

  public ArrayList<Movie> getWatchList() {
    return watchList;
  }
}

public class ProjekTubes {
  public static void main(String[] args) {
    Movie movie1 = new Movie("The Dark Knight", "Action", 2008, "Christopher Nolan");
    Movie movie2 = new Movie("Inception", "Sci-Fi", 2010, "Christopher Nolan");
    Movie movie3 = new Movie("The Shawshank Redemption", "Drama", 1994, "Frank Darabont");

    User user1 = new User("Marsudin", 25);
    User user2 = new User("Jandia", 16);

    user1.addMovieToWatchList(movie1);
    user1.addMovieToWatchList(movie2);
    user1.addMovieToWatchList(movie3);
    System.out.println(" ");
    user2.addMovieToWatchList(movie1);
    user2.addMovieToWatchList(movie2);
    user2.addMovieToWatchList(movie3);
    System.out.println(" ");
    
    System.out.println(user1.getNama() + "'s watch list:");
    for (Movie movie : user1.getWatchList()) {
      System.out.println(movie.getJudul());
    }
    System.out.println(" ");
    System.out.println(user2.getNama() + "'s watch list:");
    for (Movie movie : user2.getWatchList()) {
      System.out.println(movie.getJudul());
    }
  }
}
