class Penduduk {
    String nama;
    boolean buangSampahSembarangan;
    int penambahanVolume;
    Kali kali;

    Penduduk(String nama, boolean buangSampahSembarangan, int penambahanVolume) {
      this.nama = nama;
      this.buangSampahSembarangan = buangSampahSembarangan;
      this.penambahanVolume = penambahanVolume;
    }

    void buangSampahSembarangan(Kali kali){
      this.kali = kali;
    }

    void display(){
      kali.display();
      System.out.println(this.nama + " membuang sampah ke " + kali.nama);
      kali.tambahVolume(this.penambahanVolume);
    }
  }
  
class Kali {
    String nama;
    int volume;
    boolean meluap;
  
    Kali(String nama,int volume) {
      this.nama = nama;
      this.volume = volume;
    }
  
    void tambahVolume(int tambahanVolume) {
      this.volume += tambahanVolume;
      if (volume >= 1000) {
        meluap = true;
        System.out.println("Volume " + volume + " Kubik , " + "Melebihi batas Maksimal!!");
        System.out.println("Warning : SUNGAI MELUAP");
        System.out.println("SEGERA EVAKUASI!!!!");
      }else{
        System.out.println("Volume sungai naik !!");
        System.out.println( "Volume : " + volume + " Kubik");
        System.out.println("Sungai tidak meluap");
        System.out.println("SEGERA BERSIHKAN AGAR TIDAK MELUAP!!\n");
      }
    }

    void display(){
      System.out.println(this.nama);
      System.out.println("Volume Maksimal : 1000 kubik");
      System.out.println("Volume saat ini : " + this.volume + " Kubik");
      System.out.println("===============================");
    }
  }
  
public class Main {
    public static void main(String[] args) {
      System.out.println("\n<<<  KELAKUAN WARGA INDOGAKURE >>>\n");
      // Object Penduduk
      Penduduk penduduk1 = new Penduduk("Udin petot", true, 100);
      Penduduk penduduk2 = new Penduduk("Idan geksor", true, 200);
      Penduduk penduduk3 = new Penduduk("Icih dimir", true, 300);
      // Object Kali 
      Kali kali = new Kali("Sungai Cirawet",500);

      //Object display
      System.out.println("ORANG KE-1");
      penduduk1.buangSampahSembarangan(kali);
      penduduk1.display();
      System.out.println("ORANG KE-2");
      penduduk2.buangSampahSembarangan(kali);
      penduduk2.display();
      System.out.println("ORANG KE-3");
      penduduk3.buangSampahSembarangan(kali);
      penduduk3.display();

    }
  }